ifneq ("$(LOCAL_HOST_MODULE)","")
  $(error $(LOCAL_PATH): UBOOT not supported for host modules)
endif

###############################################################################
# Linux kernel.
###############################################################################

ifeq ("$(LOCAL_MODULE)", "uboot")

# Override the module name...
LOCAL_MODULE_FILENAME := $(LOCAL_MODULE).done

LOCAL_MODULE_CLASS := UBOOT

# General setup
UBOOT_DIR := $(LOCAL_PATH)
UBOOT_BUILD_DIR := $(call local-get-build-dir)

# Allows kernel to be of a different architecture
# ex: aarch64 for kernel and arm for system
ifndef TARGET_UBOOT_ARCH
  TARGET_UBOOT_ARCH := $(TARGET_ARCH)
endif

# Linux Toolchain
ifndef TARGET_UBOOT_CROSS
  TARGET_UBOOT_CROSS := $(TARGET_CROSS)
endif

# UBOOT_SRCARCH is the name of the sub-directory in linux/arch
ifeq ("$(TARGET_UBOOT_ARCH)","x64")
  UBOOT_ARCH := x86_64
  UBOOT_SRCARCH := x86
else ifeq ("$(TARGET_UBOOT_ARCH)","aarch64")
  UBOOT_ARCH := arm64
  UBOOT_SRCARCH := arm64
else
  UBOOT_ARCH := $(TARGET_UBOOT_ARCH)
  UBOOT_SRCARCH := $(TARGET_UBOOT_ARCH)
endif

# How to build
UBOOT_MAKE_ARGS := \
	ARCH="$(UBOOT_ARCH)" \
	CROSS_COMPILE="$(TARGET_UBOOT_CROSS)" \
	-C $(LOCAL_PATH) \
	O="$(UBOOT_BUILD_DIR)" \
	$(TARGET_UBOOT_MAKE_BUILD_ARGS)

# Register in the system
$(module-add)

endif
